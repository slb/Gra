package Test;

import java.util.List;

import dao.dao.UavDao;
import dao.factory.DaoFactory;
import dao.vo.Uav;

public class UavTest {

	public static void main(String[] args) {
		UavDao  uavDao = DaoFactory.getUavDaoInstance();
		List<Uav> uavList = uavDao.getAllUavs();
		System.out.println("*************获取所有用户的观看记录****************总条数：" + uavList.size());
		for (int i = 0; i < uavList.size(); i ++) {
			System.out.println(uavList.get(i).getUid() + "|" +  uavList.get(i).getVid());
		}
		
		String tid = "t1000";
		List<Uav> uavList2 = uavDao.getByType(tid);
		System.out.println("*********根据类型****获取所有用户的观看记录****************从条数：" + uavList2.size());
		for (int i = 0; i < uavList2.size(); i ++) {
			System.out.println(uavList2.get(i).getUid() + "|" +  uavList2.get(i).getVid());
		}
		
	}

}
